﻿function getQuotation(resposta) {
    var coins = getAllCoins();
    var message = "<p>Informações atuais:</p>";
    var image = "";
    for (var i = 0; i < coins.length; i++) {
        switch (coins[i]) {
            case 'BTC':
                message += "<p>Moeda: " + resposta.valores.BTC.nome + " - Valor: " + resposta.valores.BTC.valor + "</p>";
                break;
            case 'USD':
                message += "<p>Moeda: " + resposta.valores.USD.nome + " - Valor: " + resposta.valores.USD.valor + "</p>";
                break;
            case 'EUR':
                message += "<p>Moeda: " + resposta.valores.EUR.nome + " - Valor: " + resposta.valores.EUR.valor + "</p>";
                break;
            case 'GBP':
                message += "<p>Moeda: " + resposta.valores.GBP.nome + " - Valor: " + resposta.valores.GBP.valor + "</p>";
                break;
            case 'ARS':
                message += "<p>Moeda: " + resposta.valores.ARS.nome + " - Valor: " + resposta.valores.ARS.valor + "</p>";
                break;
            default:
                message+= "<p>Valor não encontrado</p>";
                break;
        }
    }
    $('.informationCenter').html('').html(message);
}

function findAllFields() {
    var coins = [];
    $(".fore-control").each(function () {
        if ($(this).val() !== "") {
            coins.push($(this).val());
        }
    });
    $(".selectInvisibleOne").each(function () {
        if ($(this).val() !== "") {
            coins.push($(this).val());
        }
    });
    $(".selectInvisibleTwo").each(function () {
        if ($(this).val() !== "") {
            coins.push($(this).val());
        }
    });
    return coins;
}

function configParms(arr) {
    var parm = "";
    for (var i = 0; i < arr.length; i++) {
        if (i !== (arr.length - 1)) {
            parm += arr[i] + ',';
        } else {
            parm += arr[i];
        }
    }
    return parm;
}

function getAllCoins() {
    var coins = findAllFields();
    return coins;
}

function getParms() {
    var coins = getAllCoins();
    var parm = configParms(coins);
    return parm;
}

function mountURL() {
    var apiURL = "http://api.promasters.net.br/cotacao/v1/valores?moedas=";
    var parm = getParms();
    var format = "&alt=json";
    var reqURL = apiURL + parm + format;
    return reqURL;
}

function getApiValues(url) {
    $.get(url, function (res, status) {
        console.log(status);
        if (status === 'success') {
            console.log(res);
            if (res.status == false) {
                console.log("Lamentamos o ocorrido!");
                $("#titleValorNull").text("Moeda não inserida! ");
            }
            getQuotation(res);
        } else {
            console.log("Lamentamos o ocorrido!");
        }
    });
}

function actions() {

    $("#buttonVerification").click(function () {
        apiURL = mountURL();
        getApiValues(apiURL);
    });

    var incrementButtonAdd = 0;
    $("#buttonAdd").click(function () {
        incrementButtonAdd++;
    
        if (incrementButtonAdd == 1) {
            $(".selectInvisibleOne").css("visibility", "visible");
        }

        if (incrementButtonAdd == 2) {
            $(".selectInvisibleTwo").css("visibility", "visible");
            $("#buttonAdd").css("visibility", "hidden");
        }

    });
}

$(document).ready(function () {
    actions();
});
